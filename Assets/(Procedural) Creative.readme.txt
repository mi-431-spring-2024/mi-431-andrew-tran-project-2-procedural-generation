Project - Procedural Animation (Research)

Your Name - Andrew Tran

Date - 3/4/2024

What was/were your starting tutorial(s)?
https://www.youtube.com/watch?v=acMK93A-FSY&t=237s 
 

What did you do in your Research Phase?
- Learned the basics of procedural animation
- Basics of setting up animation rig

What did you do in your Creative Phase?
Used my model from Project 1 for the purposes of procedural animation.


Any assets used that you didn't create yourself?  (art, music, etc. Just tell us where you got it, link it here)
https://on.unity.com/3jX6PAY 
(Enemy Walker model and script)



Did you receive help from anyone outside this class?  (list their names and what they helped with)



Did you get help from any AI Code Assistants?  (Tell us which .cs file to look in for the citation and describe what you learned; also be sure to comment in the .cs per the syllabus instructions)



Did you get help from any additional online websites, videos, or tutorials?  (link them here)
https://www.youtube.com/watch?v=FXhjhlNvvfw  
https://www.youtube.com/watch?v=Kk0xN26ICLQ&t=31s  


What trouble did you have with this project?
- Trying to figure out which properties to have for a gameobject when 
making procedural animation
- Animations are not fluid 
- Code used for the enemy walker doesn't work for my model. Next time, I'll use it as a reference 
when writing the code from scratch


Is there anything else we should know?

