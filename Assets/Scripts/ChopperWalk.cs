using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChopperWalk : MonoBehaviour
{
    // Gets position of model body
    public Transform body;
    public ChopperWalk otherFoot;
    
    // Space between the legs
    float footSpacing = 10f;
    // Space between arms
    // float armSpacing = 20f;
    // Makes it able to walk on terrain
    public LayerMask terrainLayer;

    // How much distance you can walk
    float stepDistance = 0.5f;
    // How much the leg brings up when walking
    float stepHeight = 1.5f;
    // How fast walking is
    float speed = 1;

    // Used to determine 2 points of vectors
    float lerp;
    // Store values for position
    Vector3 currentPos, newPos, oldPos;
    // Start is called before the first frame update
    void Start()
    {
        // rightLegRotate = rightLeg.transform.rotation;
        // Captures spacing of legs based on x position
        footSpacing = transform.localPosition.x;
        // armSpacing = transform.localPosition.z;
        // Each position value is equal
        currentPos = newPos = oldPos = transform.position;
        lerp = 1;
       
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = currentPos;
        // Uses ray casting to make the model move up and down but the legs stick to the ground
        Ray ray = new Ray(body.position, Vector3.down);
        // If the model starts to walk
        if (Physics.Raycast(ray, out RaycastHit info, 3, terrainLayer.value))
        {
            // New position is recorded when going over how much distance is walked
            if (Vector3.Distance(newPos, info.point) > stepDistance && !otherFoot.isMoving() && lerp >= 1)
            {
                lerp = 0;
                newPos = info.point;
            }
        }
        // Adjusts the position of legs when reaching this condition
        if (lerp < 1)
        {
            Vector3 footPos = Vector3.Lerp(currentPos, newPos, lerp);
            footPos.z += Mathf.Cos(lerp * Mathf.PI) * stepHeight;
            currentPos = footPos;
            lerp += Time.deltaTime * speed;
        }
        else
        {
            oldPos = newPos;
        }
        
    }

    public bool isMoving()
    {
        return lerp < 1;
    }
    // Draws spheres to allow the user to know where the foot will land next on the terrain
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(newPos, 0.2f);

    }
}
