using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LegAnimations : MonoBehaviour
{
    // Gets position of model body
    public Transform body;
    // Space between the legs
    float footSpacing = 5f;
    // Makes it able to walk on terrain
    public LayerMask terrainLayer;

    // How much distance you can walk
    float stepDistance = 3;
    // How much the leg brings up when walking
    float stepHeight = 3;
    // How fast walking is
    float speed = 5;

    // Used to determine 2 points of vectors
    float lerp;
    // Store values for position
    Vector3 currentPos, newPos, oldPos;
    bool isMoving = false;
    // Start is called before the first frame update
    void Start()
    {
        // Captures spacing of legs based on x position
        footSpacing = transform.localPosition.x;
        // Each position value is equal
        currentPos = newPos = oldPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = currentPos;
        // Uses ray casting to make the model move up and down but the legs stick to the ground
        Ray ray = new Ray(body.position + (body.right * footSpacing), Vector3.down);
        // If the model starts to walk
        if (Physics.Raycast(ray, out RaycastHit info, 10, terrainLayer.value))
        {
            // New position is recorded when going over how much distance is walked
            if (Vector3.Distance(newPos, info.point) > stepDistance)
            {
                lerp = 0;
                newPos = info.point;
                // isMoving = true;
            }
        }
        // Adjusts the position of legs when reaching this condition
        if (isMoving == true)
        {
            Vector3 footPos = Vector3.Lerp(currentPos, newPos, lerp);
            footPos.y += Mathf.Sin(lerp * Mathf.PI) * stepHeight;
            currentPos = footPos;
            lerp += Time.deltaTime * speed;
        }
        else
        {
            oldPos = newPos;
        }
        
    }

    // Draws spheres to allow the user to know where the foot will land next on the terrain
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(newPos, 5f);

    }
}
